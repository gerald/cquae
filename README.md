# CQuAE : A new Contextualised QUestion Answering corpus on Education domain
The first an seconde version of the corpus is available. Below you will find a short description of the corpus (data/v1).
Notice that scripts and other data (anonymised) will be progressively added.


## Description of the json annotation file
The json annotation file contains the annotation list. Each item of the list corresponds to the different field collected that are:
* **user (str)**: The author of the annotation, notice as it is an anonymised version, authors correspond only to a number "author-number". However it is still possible to observe annotation made by a same author
* **qid (int)**: The question/annotation unique identifier 
* **did (int)**: The document unique identifier, each annotation is based upon a document (multiple paragraphes). The document collection will be described in a later section.
* **question (str)**: The question corresponding to the current annotation.
* **question_pids (list)**: The list of the different paragraphs, where at least one question_spans have been selected (usualy for question only one paragraph)
* **question_spans (list)**: The list of spans selected for the question (spans on what rely the question, not necessarly containing the answer). Each "spans" contains a dictionary containing
    * **start_token (int)**: Location of the begining of the selection in the paragraph
    * **end_token (int)**: Location of the end of the selection in the paragraph
    * **text (str)**: The text associated to the selection
    * **pid (int)**: The paragraph identifier where the selection is based upon
* **question_type (str)**: The type of the question among "factual", "definition", "course" or "synthesis"
* **answer_pids (list)**: The paragraphs identifier where a part of the answer is selected
* **answer_spans (list)**: The list of spans selected for the answer (spans that contain a part of the answer)
* **answer (str)**: The answer written by the annotator
* **date (str)**: The date corresponding to the creation of the annotation (notice only year, month and day are given)


Below there is an exemple of an entry of the annotation file :

```
{
        "user": "author-0",
        "qid": 1,
        "did": 212,
        "question": "Qui est Madame Roland ?",
        "question_pids": [
            0
        ],
        "question_spans": [
            {
                "start_token": 1,
                "end_token": 52,
                "text": "Jeanne-Marie Phlipon, dite Manon Roland (1754‑1793)",
                "pid": 0
            }
        ],
        "question_type": "course",
        "answer_pids": [
            0
        ],
        "answer_spans": [
            {
                "start_token": 1,
                "end_token": 391,
                "text": "Jeanne-Marie Phlipon, dite Manon Roland (1754‑1793), est issue de la bourgeoisie. Particulièrement brillante, elle reçoit une éducation de haut niveau. Lorsque survient la Révolution, elle s’implique avec enthousiasme dans le mouvement. Elle y joue un rôle clé, par l’intermédiaire de son mari, Jean‑Marie Roland, élu député puis nommé ministre, en corédigeant notamment tous ses discours. ",
                "pid": 0
            }
        ],
        "answer": "Manon Roland (1754-1793) est une femme ayant participé activement à la révolution Française issue de la bourgeoisie. Elle est notamment connue pour son implication dans la révolution Française a travers la diffusion d'idées via les salons Girondins et la rédaction de discours avec son mari Jean-Marie Rolant.",
        "date": "2022-12-14"
    }
```

At the current time, if you want to prepare data (or prepare some test) you can use the ``cquae-sample-annotation.json`` file in the samples folder.

The complete corpus file is given in ``cquae-annotation.json`` in the data directory.



## Description of documents json file

All of the previous annotation rely on a textual document. Documents can be find in the `cquae-document.json`` file. This file is a list of document (dictionary) where the field **"did"** correspond to the index of the document (did field is referenced in each annotation).

The format of a document is described below :
* **url (str)**: The source url of the document 
* **title (str)**: The title of the document (for wikipedia the title of the page for "le livre scolaire" the name of the lesson)
* **data (list)**: A list of dictionnary with the content of the document, each dictionary correspond to a paragraph or section of the document, where each dictionary is as follow :
    * **source_text (str)** : The textual content of the paragraph
    * **title(str)**: The title of the section (empty string if the section does not have a title) 
    * **img(list)**: List of string being the url of the images. The list is empty if there is no image for the section


```
{
    "url": "https://www.lelivrescolaire.fr/page/6330739",
    "title": "L’organisme pluricellulaire, ensemble de cellules spécialisées",
    "data": [
        {
            "source_text": " On peut observer, à l’œil nu, un organisme pluricellulaire et les organes qui le constituent. Ces organes sont regroupés en appareils qui assurent différentes grandes fonctions dans l’organisme.\n\nAvec un microscope optique, on peut observer les tissus qui constituent les organes et les cellules qui composent ces tissus.\n\nAvec un microscope électronique, il est possible d’observer les compartiments qui se trouvent dans les cellules, les organites.\n\nDes technologies récentes permettent de visualiser les molécules contenues dans la cellule ou dans ses organites, et des outils numériques permettent de modéliser la structure tridimensionnelle de ces molécules.",
            "title": "1 Les échelles du vivant [activité 1]",
            "img": []
        },
        {
            "source_text": " Chez les organismes unicellulaires, toutes les fonctions sont assurées par une seule cellule.\n\nChez les organismes pluricellulaires, les cellules constituant les organes sont dites spécialisées. Elles ont des formes, des tailles et des positions différentes. Elles peuvent aussi posséder des organites et des molécules spécifiques.\n\nLa spécialisation cellulaire permet donc aux cellules de réaliser des fonctions particulières.",
            "title": "2 La spécialisation des cellules [activité 2 et activité numérique]",
            "img": []
        },
        {
            "source_text": " Entre les cellules des organismes pluricellulaires, il existe un réseau de molécules, de nature chimique variée, qui forment ce que l’on appelle la matrice extracellulaire. Chez les végétaux, cette matrice extracellulaire est appelée la paroi.\n\nLes molécules qui composent ces matrices permettent l’adhérence entre les cellules et ainsi la formation de tissus cohérents. Elles permettent aussi la protection des cellules, le maintien de leur forme ou bien la communication entre elles.",
            "title": "3 Les matrices extracellulaires [activité 3]",
            "img": []
        },
        {
            "source_text": " Organisme : être vivant.\n\nPluricellulaire : qui comprend plusieurs cellules (en opposition à unicellulaire).\n\nOrgane : ensemble structural et fonctionnel de cellules, qui remplit une certaine fonction à l’échelle de l’organisme.\n\nTissu : ensemble cohérent de cellules qui présentent une structure et une fonction similaires.\n\nCellule : unité structurale et fonctionnelle du vivant, délimitée par une membrane plasmique.\n\nOrganite : structure intracellulaire délimitée par au moins une membrane biologique, et assurant une (ou des) fonction(s) précise(s).\n\nUnicellulaire : organisme qui n’est composé que d’une seule cellule.\n\nSpécialisation : présentation d’une structure en lien avec une fonction particulière.\n\nMatrice extracellulaire : ensemble de macromolécules sécrétées par une cellule et qui composent son environnement immédiat. Elle est appelée paroi chez les végétaux.\n\nParoi : nom donné à la matrice extracellulaire végétale. Peut plus largement correspondre à toute structure rigide protégeant une cellule (parois bacérienne ou fongique).",
            "title": "Mots-clés ",
            "img": []
        },
        {
            "source_text": " Lorsque l'on se muscle, les efforts répétés que nous faisons envoient un signal de différenciation aux cellules souches (les cellules satellites), ce qui permet à de nouvelles cellules musculaires d'être formées. Ceci contribue à augmenter la taille du muscle.",
            "title": "Au quotidien",
            "img": []
        }
    ],
    "collection": "lls-svt-seconde",
    "did": 10
}
```


## The second version of the dataset

With the evaluation of the first dataset, we observed some issues in the content, some question or answer contains errors (typo) or are few relevant for education. We thus proposed to start a second campaign to correct the corpus, with two major sub-task :
* Judging the relevance of the v1 data/annotation
* Correct the unrelevant data/annotation

For further explanation please read the specific [readme.md](data/v2/readme.md). 

For scripts to reproduce the results presented in the paper you can read the specific [readme.md](scripts/readme.md). 

## Evaluation scripts
The evaluation scrits can be find in the [`./scripts`](./script). 

## Training/fine-tuning

The fine-tuning as been performed using [axolotl](https://github.com/axolotl-ai-cloud/axolotl) library(no code). We provide the script for inference in the TALN folder and the different configuration fine-tuning files for TALN paper results (see [taln training folder](./TALN-models)).

## Annotation platform
For the annotation we used an home made annotation platform, however this platform will not be published soon. For the evaluation of te annotation we use the doccano annotation platform available at [https://github.com/doccano/doccano](https://github.com/doccano/doccano).

## Citation
```
@inproceedings{gerald:hal-04623009,
  TITLE = {{CQuAE : Un nouveau corpus de question-r{\'e}ponse pour l'enseignement}},
  AUTHOR = {Gerald, Thomas and Tamames, Louis and Ettayeb, Sofiane and Paroubek, Patrick and Vilnat, Anne},
  URL = {https://inria.hal.science/hal-04623009},
  BOOKTITLE = {{35{\`e}mes Journ{\'e}es d'{\'E}tudes sur la Parole (JEP 2024) 31{\`e}me Conf{\'e}rence sur le Traitement Automatique des Langues Naturelles (TALN 2024) 26{\`e}me Rencontre des {\'E}tudiants Chercheurs en Informatique pour le Traitement Automatique des Langues (RECITAL 2024)}},
  ADDRESS = {Toulouse, France},
  EDITOR = {BALAGUER and Mathieu and BENDAHMAN and Nihed and HO-DAC and Lydia-Mai and MAUCLAIR and Julie and MORENO and Jose G and PINQUIER and Julien},
  PUBLISHER = {{ATALA \& AFPC}},
  VOLUME = {1 : articles longs et prises de position},
  PAGES = {50-63},
  YEAR = {2024},
  MONTH = Jul,
  KEYWORDS = {question-r{\'e}ponse ; corpus ; {\'e}ducation},
  PDF = {https://inria.hal.science/hal-04623009/file/6245.pdf},
  HAL_ID = {hal-04623009},
  HAL_VERSION = {v1},
}

```

```
@inproceedings{gerald-etal-2024-introducing-cquae,
    title = "Introducing {CQ}u{AE} : A New {F}rench Contextualised Question-Answering Corpus for the Education Domain",
    author = "Gerald, Thomas  and
      Vilnat, Anne  and
      Ettayeb, Sofiane  and
      Tamames, Louis  and
      Paroubek, Patrick",
    editor = "Calzolari, Nicoletta  and
      Kan, Min-Yen  and
      Hoste, Veronique  and
      Lenci, Alessandro  and
      Sakti, Sakriani  and
      Xue, Nianwen",
    booktitle = "Proceedings of the 2024 Joint International Conference on Computational Linguistics, Language Resources and Evaluation (LREC-COLING 2024)",
    month = may,
    year = "2024",
    address = "Torino, Italia",
    publisher = "ELRA and ICCL",

    pages = "9234--9244",
}
```