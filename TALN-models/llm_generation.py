import json
import codecs
import torch
from argparse import ArgumentParser
from tqdm import tqdm
from pathlib import Path
from transformers import AutoTokenizer, AutoModelForCausalLM
from peft import PeftModel
from datetime import datetime

# No changes needed in this function, but can be updated if required
def process_in_batches(eval_data, batch_size):
    for itr in range(0, len(eval_data), batch_size):
        yield eval_data[itr:itr + batch_size]

def process_batch(data_batch, tokenizer, model, args):
    inputs = tokenizer(data_batch, return_tensors="pt", padding=True).to(args.device)

    generation_output = model.generate(
        **inputs,
        max_new_tokens=args.max_new_tokens,
        pad_token_id=tokenizer.eos_token_id
    )

    return tokenizer.batch_decode(generation_output, skip_special_tokens=True)

def AR_data(batch):
    for datum in batch:
        if datum['dataset'] == 'answer_reformulate':
            return True
    return False

def generate(model, tokenizer, eval_data, generated_data, ckpt, args):

    # Modification to the printout for accounting the start index
    print(f'Evaluation will take {(len(eval_data) - args.start_at) // args.batch_size} batches.')
    for batch in tqdm(process_in_batches(eval_data[args.start_at:], args.batch_size)):

        data_batch = [args.input_format.format(instruction=datum['input']) for datum in batch]

        if AR_data(batch):
            for mini_batch in process_in_batches(batch, args.batch_size//4):
                mini_data_batch = [args.input_format.format(instruction=datum['input']) for datum in mini_batch]
                generated = process_batch(mini_data_batch, tokenizer, model, args)
                generated_data.extend(
                    [
                        {
                            **mini_batch[u],
                            # ckpt: gene[len(batch[u]['input']):].strip()
                            ckpt: gene.split(args.split_answer)[1].strip()
                            } for u, gene in enumerate(generated)
                    ]
                )
        else:
            generated = process_batch(data_batch, tokenizer, model, args)
            generated_data.extend(
                [
                    {
                        **batch[u],
                        # ckpt: gene[len(batch[u]['input']):].strip()
                        ckpt: gene.split(args.split_answer)[1].strip()
                        } for u, gene in enumerate(generated)
                ]
            )

        with codecs.open(args.out_dir / (args.out_file + ckpt + '.json'), "w", encoding='utf8') as f:
            json.dump(generated_data, f, ensure_ascii=False)
        
    return generated_data

def generate_multiple_checkpoints(args):
    # Load the data
    with open(args.data_path, 'r') as f:
        eval_data = json.load(f)

    # Load the model
    if args.tokenizer_name:
        tokenizer = AutoTokenizer.from_pretrained(args.tokenizer_name, trust_remote_code=True)
    else:
        tokenizer = AutoTokenizer.from_pretrained(args.model_name, trust_remote_code=True)
    tokenizer.pad_token = tokenizer.eos_token
    if args.left_padding:
        tokenizer.padding_side  = 'left'

    model_args = {
        'pretrained_model_name_or_path': args.model_name,
        'torch_dtype': torch.bfloat16,
        'device_map': args.device,
        'trust_remote_code': True
    }

    if args.load_in_8bit:
        model_args['load_in_8bit'] = True

    if args.load_in_4bit:
        model_args['load_in_4bit'] = True

    # if args.attn:
    #     model_args['attn_implementation'] = args.attn #'flash_attention_2'

    model = AutoModelForCausalLM.from_pretrained(**model_args)

    # Load previous data if the generation run stopped
    if args.start_at == 0:
        start_at_first = False
    else:
        with codecs.open(args.out_dir / (args.out_file + '.json'), 'r') as f:
            generated_data = json.load(f)
        print(generated_data[-1])
        start_at_first = True

    if args.no_adapter:
        print(f'Processing checkpoint: no_adapter')
        eval_data = generate(
            model = model,
            tokenizer=tokenizer,
            eval_data=eval_data,
            generated_data=generated_data if start_at_first else [],
            ckpt = 'no_adapter',
            args = args
            )
        start_at_first = False

    for checkpoint in args.checkpoint_paths:
        print(f'Processing checkpoint: {checkpoint}')
        generation_model = PeftModel.from_pretrained(model, checkpoint)
        # args.out_file = f'generated_data_{checkpoint.name}'  # Set output file based on checkpoint name
        eval_data = generate(
            model = generation_model,
            tokenizer=tokenizer,
            eval_data=eval_data,
            generated_data=generated_data if start_at_first else [],
            ckpt = checkpoint.stem,
            args = args
            )
        start_at_first = False

    return eval_data

if __name__ == '__main__':
    parser = ArgumentParser()

    ### EVALUATION PARAMETERS
    parser.add_argument('--input_format', type=str, default="[INST] {instruction} [/INST]")
    parser.add_argument('--split_answer', type=str, default="[/INST]")
    parser.add_argument('--batch_size', type=int, default=24,
                        metavar='batchsize to generate samples')
    parser.add_argument('--max_new_tokens', type=int, default=650,
                        metavar='maximum token generated by the model')
    parser.add_argument('--device', type=str, default='cuda:0')
    
    ### DATA
    parser.add_argument('--model_name', type=str, default="mistralai/Mistral-7B-Instruct-v0.1")
    parser.add_argument('--tokenizer_name', type=str, default=None)
    parser.add_argument('--no_adapter', action="store_true", help="Activate generation on original model")
    parser.add_argument('--checkpoint_paths', type=Path, nargs='*', default=[],
                        help='List of adapter checkpoint paths')
    parser.add_argument('--load_in_8bit', action="store_true", help="Activate load in 8bit")
    parser.add_argument('--load_in_4bit', action="store_true", help="Activate load in 4bit")
    # parser.add_argument('--attn', type=str, default=None, help="to put flash attn: 'flash_attention_2'")
    parser.add_argument('--left_padding', action="store_true", help="Set padding to left")
    
    
    parser.add_argument('--data_path', type=Path)
    parser.add_argument('--out_dir', type=Path, default=Path("./"))
    parser.add_argument('--out_file', type=str)

    parser.add_argument('--start_at', type=int, default=0,
                        help='datum to start generating from (for resuming), for first checkpoint pas, or no_adapter if set')
    
    args = parser.parse_args()

    print('Start: ', datetime.now().strftime("%H:%M:%S"))

    total_data = generate_multiple_checkpoints(args)
    
    with codecs.open(args.out_dir / (args.out_file + '.json'), "w", encoding='utf8') as f:
        json.dump(total_data, f, ensure_ascii=False)
    
    print('End: ', datetime.now().strftime("%H:%M:%S"))
