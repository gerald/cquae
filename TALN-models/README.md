## Adapting a model 
To adapt a model you can run the training script using the (`axolotl` framework)[https://github.com/axolotl-ai-cloud/axolotl] :

``accelerate launch -m axolotl.cli.train training-config/llama/taln_ragv1.yml``
You may have to replace the path of the training set (see in data/taln).


## Generating scores
To generate scores you can use the script  `llm_generation.py`
