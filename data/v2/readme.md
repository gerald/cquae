# Second version of CQUAE corpus

A second version of the corpus will be shared soon. This second version rely on a correction of the original corpus.

## Judging relevance of the annotation
 A first step to correct the corpus rely on judging the relevance of the question and the answer.

 To judge of the relevance we ask annotators to judge questions and answers given the context (paragraphs selected by annotors in the first step).
 We ask them to select the relevants binary criteria beyond : 
 * **Question is correct (QC)**
 * **Question need to be reviewed or corrected (QR)**
 * **Question is irrelevant for education purpose or not understandable (QI)**
 * **Answer is correct (AC)**
 * **Answer need to be reviewed or corrected (AR)**

On the 11272 judged documents (some are given to many annotators) we obtained the following results : 

| Criterion | ALO | T |
| -------- | ------------------ | ------------ |
| QC | 7833 (69.5%) | 6817 (60.4%) |
| QR | 3193 (28.3%) | 2524 (22.4%) |
| QI | 1698 (15.0%) | 500 (4.4%) |
| AC | 6830 (60.6%) | 5783 (51.3%) |
| AR | 4219 (37.4%) | 3469 (30.8%)|

With : 

* **ALO** : Number of document/annotation where at least one evaluator selected the criterion
* **T** : Number of document/annotation where all annotators selected the criterion (notice that most of the annotations only have 1 reviewer)

### Understanding scores
To understand criteria we conducted small experiments, particularly to estimate how criteria correlate to questions or answers structures.

A first question is **"Does correctness of an annotation correlate with the size of the question or the answer?"**

In the figure below we evaluate the correctness of both the question and the answer according to the length of the question (using mean decile).
The tendency of both questions and answers seems to show that more the question is short, less it is likely to be uncorrect.

![alt text](../../figures/correctness_question-size.svg)

Interestingly, this property is not preserved looking at the answer length. At the contrary, the answer and the question are unlikely to be uncorrect if the answer is long.

![alt text](../../figures/correctness_answer-size.svg)


### Question length and answer length correlation

![alt text](../../figures/corelation_qlen_rlen.svg)

![alt text](../../figures/corelation_rlen_qlen.svg)