# Evaluation scripts for the CQuAE paper 

Link to the paper (in french) : [https://hal.science/LISN/hal-04623009v1](https://hal.science/LISN/hal-04623009v1)
## Words statistic on the corpus 

The notebook `TALN-word-corpus-statistic.ipynb` is the file used to create the words statistics (page 7 to 8). It contains the scripts to :
* Get query words charts (figure 2 of the article)
* The size of the answers and questions (figure 1 of the article)

## Corpus Evaluation Campaign
The results of the evaluation campaigns are available in the `data/review` folder. Those results have been obtained using [doccano](https://github.com/doccano/doccano). Notice that a reviewer have been discarded for the second version of the corpus.

The script to obtain results presented in the Table 5 of the article is `TALN-statistic-annotation-judgement.ipynb`. Due to time/budget constraints not all question and answer have been evaluated.

## Evaluation of the RAG models
The scores for the RAG models can be retrieved usig the [`TALN-eval-rag.ipynb`script](./TALN-eval-rag.ipynb). The prediction file are located in [the data folder](../data/prediction).
Notice that the training of the models is not yet available on the plateform

## Human evaluation of the RAG models
This part correspond to the LREC paper with judgement of the generation. Notice however that the testing set is different from the TALN paper.
